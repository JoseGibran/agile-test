import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';

class App extends React.Component {

  constructor(props){
    super(props);
    this.htmlOutput = '';
    this.tooltipActive = false;
  }


  componentDidMount(){
  
  }

  applyBold = () => {
    document.execCommand('bold',false,null);
  }

  applyItalic = () => {
    document.execCommand('italic',false,null);
  }
  applyUnderline = () => {
    document.execCommand('underline',false,null);
  }
  applyCutText = () => {
    document.execCommand('cut', false, null);
  }

  applySmallText = () => {
    document.execCommand('fontSize', false, 2);
  }

  applyBigText = () => {
    document.execCommand('fontSize', false, 7);
  }

  displayText = (event) => {
    const HTMLCode = this._richtextbox.innerHTML;
    this.htmlOutput = HTMLCode;
    
  }

  getSynonimous = async (event)  =>{
    
    if(this.tooltipActive){
      ReactDOM.unmountComponentAtNode(this._tooltip);
    }
    
    const selectedText = window.getSelection().toString();
    if(selectedText){
      const x = event.clientX, y = event.clientY;  
      this._tooltip.style.top = (y + 20) + 'px';
      this._tooltip.style.left = (x + 20) + 'px';
      this._tooltip.style.display = 'inherit';
      this._tooltip.innerHTML = 'Cargando..';

      let response = await fetch(`https://api.datamuse.com/words?ml=${selectedText}`);
      response = await response.json();
      
      if(response[0]){
        this.tooltipActive = true;
        ReactDOM.render(<ul>
          {response[0] && <li><a href="javascript;;" onClick={(event) => this.replaceWord(event, response[0].word)}>{response[0].word}</a></li>}
          {response[1] && <li><a href="javascript;;" onClick={(event) => this.replaceWord(event, response[1].word)}>{response[1].word}</a></li>}
          {response[2] && <li><a href="javascript;;" onClick={(event) => this.replaceWord(event, response[2].word)}>{response[2].word}</a></li>}
        </ul>, this._tooltip);
      }else{
        this._tooltip.innerHTML = 'No results found.'
      }
     
    
    }
  }

  replaceWord = (event, word) => {
    event.preventDefault();
    ReactDOM.unmountComponentAtNode(this._tooltip);
    this._tooltip.style.display = 'none';
    const selectedText = window.getSelection().toString();
    this._richtextbox.innerHTML = this.htmlOutput.replace(selectedText, word + " ");
    this.htmlOutput = this.htmlOutput.replace(selectedText, word + " ");
  }




  componentDidUpdate(){
    
  }

  render(){
    return (
      <div>
        <h1 style={styles.title}>Text editor by Gibran!</h1>
          <div style={styles.toolbar}>
            <button onClick={this.applyBold}>B</button>
            <button onClick={this.applyItalic}>I</button>
            <button onClick={this.applyUnderline}>U</button>
            <button onClick={this.applyCutText}>cut</button>
            <button onClick={this.applySmallText}>a</button>
            <button onClick={this.applyBigText}>A</button>
          </div>
          <div 
            ref={richtextbox=>this._richtextbox = richtextbox} 
            style={styles.richTextArea} 
            contentEditable 
            onInput={event=>{ this.displayText(event) }}
            onKeyUp={this.displayText}
            onDoubleClick={this.getSynonimous}
            >
          </div>

          <div style={styles.tooltip} ref={tooltip => this._tooltip = tooltip}></div>
      </div>
    );
  }
  
}


const styles = {
  title: {
    textAlign: 'center',
  },
  toolbar: {
    padding: '0 20px',
  },
  richTextArea: {
    minHeight: 400,
    margin: 20,
    padding: 10,
    fontSize: 20,
    border: '1px solid black',
  },
  tooltip: {
    position: 'absolute',
    padding: 10,
    border: '1px solid black',
    display: 'none',

  }
}

export default App;
